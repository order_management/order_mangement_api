<?php


use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ReturnOrderController;
use App\Http\Controllers\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/customer')->controller(CustomerController::class)->group(function () {
    Route::get('', 'index');
    Route::get('/{id}', 'show');
    Route::post('/', 'store');
    Route::put('/{id}', 'update')->name('customer.update');
});

Route::prefix('/return-order')->controller(ReturnOrderController::class)->group(function () {
    Route::get('', 'index');
    Route::get('/{id}', 'show');
    Route::post('/', 'store');
});

Route::prefix('order')->group(function () {
    Route::controller(OrderController::class)->group(function () {
        Route::get('list', 'get')->name('api.order.list');
        Route::get('show/{id}', 'show')->name('api.order.show');
        Route::post('create', 'store');
        Route::put('update/{id}', 'update');
    });
});
