<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->truncate();

        for ($i = 1; $i <= 3; $i++) {
            DB::table('users')->insert([
                'name' => 'Sale ' . $i,
                'role' => 'sale',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
