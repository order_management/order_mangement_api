<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderDetailSeeder extends Seeder
{
    public function run()
    {
        DB::table('order_details')->truncate();

        for ($i = 1; $i <= 20; $i++) {
            $productPrice = rand(1000, 5000) / 100;
            $quantity = rand(1, 5);
            $discount = rand(0, 20);
            $totalPrice = $productPrice * $quantity - $discount;

            DB::table('order_details')->insert([
                'order_id' => rand(1, 10),
                'product_id' => rand(1, 10),
                'product_price' => $productPrice,
                'quantity' => $quantity,
                'status' => ['all', 'part', 'none'][array_rand(['all', 'part', 'none'])],
                'total_price' => $totalPrice,
                'discount' => $discount,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
