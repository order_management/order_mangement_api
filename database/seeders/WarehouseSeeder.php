<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WarehouseSeeder extends Seeder
{
    public function run()
    {
        DB::table('warehouses')->truncate();

        for ($i = 1; $i <= 3; $i++) {
            DB::table('warehouses')->insert([
                'name' => 'Warehouse ' . $i,
            ]);
        }
    }
}
