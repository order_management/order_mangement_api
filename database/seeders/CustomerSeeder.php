<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CustomerSeeder extends Seeder
{
    public function run()
    {
        DB::table('customers')->truncate();

        for ($i = 1; $i <= 10; $i++) {
            DB::table('customers')->insert([
                'customer_code' => 'C' . str_pad($i, 3, '0', STR_PAD_LEFT),
                'name' => 'Customer ' . $i,
                'type' => $i % 2 == 0 ? 'company' : 'self',
                'email' => 'customer' . $i . '@example.com',
                'address' => 'Address ' . $i,
                'tax_code' => $i % 2 == 0 ? 'TAX' . $i : null,
                'phone' => '555-000' . $i,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
