<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    public function run()
    {
        DB::table('products')->truncate();

        for ($i = 1; $i <= 10; $i++) {
            DB::table('products')->insert([
                'name' => 'Product ' . $i,
                'sku' => 'SKU' . str_pad($i, 3, '0', STR_PAD_LEFT),
                'discount' => $i % 2 == 0 ? rand(5, 15) : 0,
                'quantity' => rand(50, 200),
                'price' => rand(1000, 5000) / 100,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
