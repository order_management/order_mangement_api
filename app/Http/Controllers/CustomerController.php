<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Http\Services\CustomerService;
use Exception;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    protected $customerService;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * Show Customer List
     */
    public function index()
    {
        try {
            return $this->customerService->index();

        } catch (Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Show Customer Detail
     */
    public function show($id)
    {
        try {
            return $this->customerService->show($id);
        } catch (Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Create Customer
     */
    public function store(CustomerRequest $request)
    {
        try {
            $params = $request->only(['customer_code','name', 'type', 'email', 'address', 'tax_code', 'phone']);
            $newCustomer = $this->customerService->store($params);
            $id = $newCustomer->id;
            if(!$newCustomer->customer_code){
                $customerCode = 'KH_' . $id;
                $newCustomer->customer_code = $customerCode;
                $newCustomer->save();
            }
            return $newCustomer;
        } catch (Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Edit Customer
     */
    public function update(UpdateCustomerRequest $request, $id)
    {
        try {
            $customer = $this->customerService->show($id);
            $params = $request->only(['name', 'type', 'email', 'address', 'tax_code', 'phone']);

            return $this->customerService->update($customer, $params);
        } catch (Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ]);
        }
    }
}

