<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderShowResource;
use App\Http\Services\OrderService;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
     /**
     * @var Services| \App\Http\Services
     */
    protected $orderService;


    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $dataOrder = $request->only("customer_id",
                                   "sale_id", 
                                   "total_price", 
                                   "final_price", 
                                   "order_status", 
                                   "address", 
                                   "date_delivery",
                                   "note",
                                   "warehouse_id"
            );
            $dataOrderDetail = $request->only('listProducts');
            $this->orderService->store($dataOrder, $dataOrderDetail);
        } catch(Exception $e) {
            DB::rollBack();
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ]); 
        }
        return response()->json([
            'error' => false,
            'message' => "success"
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try{
            $dataOrder = $request->only("customer_id",
                                   "sale_id", 
                                   "total_price", 
                                   "final_price", 
                                   "order_status", 
                                   "address", 
                                   "date_delivery",
                                   "note",
                                   "warehouse_id"
            );
            $dataOrderDetail = $request->only('listProducts');
            $this->orderService->update($dataOrder, $dataOrderDetail, $id);
        } catch(Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ]); 
        }
        return response()->json([
            'error' => false,
            'message' => "success"
        ]);
    }
    
    /*
     * Get List Order
     * @param Request $request
     */
    public function get(Request $request)
    {
        try {
            $limit = isset($request->limit) ? $request->limit : 10;
            $orders = $this->orderService->getListPaginate($limit);

            return OrderResource::collection($orders);
        } catch (Exception $err) {
            return response()->json([
                'error' => true,
                'message' => $err->getMessage()
            ], 500);
        }
    }

    /**
     * Get Order by ID
     * @param Request $request
     */
    public function show(Request $request)
    {
        try {
            $orders = $this->orderService->getOrderById($request->id);

            return new OrderShowResource($orders);
        } catch (Exception $err) {
            return response()->json([
                'error' => true,
                'message' => $err->getMessage()
            ], 500);
        }
    }
}
