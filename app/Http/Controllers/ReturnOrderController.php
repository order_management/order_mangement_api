<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReturnOrderRequest;
use Illuminate\Http\Request;
use App\Http\Services\ReturnOrderService;
use Exception;

class ReturnOrderController extends Controller
{
    protected $returnOrderService;

    public function __construct(ReturnOrderService $returnOrderService)
    {
        $this->returnOrderService = $returnOrderService;
    }

    public function index()
    {
        try {
            return $this->returnOrderService->index();

        } catch (Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function show($id)
    {
        try {
            return $this->returnOrderService->show($id);
        } catch (Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function store(ReturnOrderRequest $request)
    {
        try {
            $params = $request->only(['customer_code', 'customer_name', 'customer_address', 'order_detail', 'note']);
            return $this->returnOrderService->store($params);
        } catch (Exception $exception) {
            return response()->json([
                'error' => true,
                'message' => $exception->getMessage()
            ]);
        }
    }
}
