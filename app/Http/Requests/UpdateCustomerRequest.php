<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $customerId = $this->route('id');

        return [
            'name' => 'required|string|max:255',
            'type' => 'required|in:company,self',
            'email' => 'required|email|unique:customers,email,' . $customerId,
            'address' => 'required|string|max:255',
            'phone' => 'required|min:10|max:11|string|unique:customers,phone,' . $customerId,
        ];
    }

    /**
     * Get the validation rules that apply to the request conditionally.
     */
    public function withValidator($validator)
    {
        $customerId = $this->route('id');

        $validator->sometimes('tax_code', 'required|string|regex:/^[A-Za-z0-9]{10}$/u|unique:customers,tax_code,' . $customerId, function ($input) {
            return $input->type === 'company';
        });

        $validator->sometimes('customer_code', 'required|string|unique:customers,customer_code' . $customerId, function ($value) {
            return $value['type'] === 'company';
        });
    }
}
