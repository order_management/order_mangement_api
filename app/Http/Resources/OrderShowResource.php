<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'order_code' => $this->order_code,
            'customer_code' => $this->customer->customer_code,
            'customer_name' => $this->customer->name,
            'customer_phone' => $this->customer->phone,
            'customer_email' => $this->customer->email,
            'customer_tax' => $this->customer->tax_code,
            'sale_name' => $this->user->name,
            'address' => $this->address,
            'note' => $this->note,
            'final_price' => $this->final_price,
            'total_paid' => round($this->payment->sum('amount'), 2),
            'order_details' => OrderDetailResource::collection($this->order_detail)
        ];
    }
}
