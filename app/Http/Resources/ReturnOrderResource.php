<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ReturnOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'order_code' => $this->order_code,
            'customer_code' => $this->customer->customer_code,
            'customer_name' => $this->customer->name,
            'customer_address' => $this->customer->address,
            'order_detail' => $this->order_detail,
        ];
    }
}
