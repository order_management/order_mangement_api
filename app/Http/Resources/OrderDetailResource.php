<?php

namespace App\Http\Resources;

use App\Models\Product;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'order' => $this->order->customer,
            'product_price' => $this->product_price,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'total_price' => $this->total_price,
            'discount' => $this->discount,
            'product_id' => $this->product_id,
            'product_sku' => $this->product->sku,
            'product_name' => $this->product->name,
            'total_price' => $this->quantity * $this->product_price
        ];
    }
}
