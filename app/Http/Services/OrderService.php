<?php

namespace App\Http\Services;

use App\Repositories\OrderRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\ProductRepository;

class OrderService extends BaseService
{
     /**
     * @var Repository| \App\Repositories
     */
    protected $orderRepo;

    /**
     * @var Repository| \App\Repositories
     */
    protected $orderDetailRepo;


    /**
     * @var Service| \App\Serive
     */
    protected $productRepo;

    public function __construct(
        OrderRepository $orderRepo,
        OrderDetailRepository $orderDetailRepo,
        ProductRepository $productRepo
    ) {
        $this->orderRepo = $orderRepo;
        $this->orderDetailRepo = $orderDetailRepo;
        $this->productRepo = $productRepo;
    }

    /**
     * Store a newly created resource in storage.
     * @param $data
     * @return mixed
     */
    public function store($dataOrder, $dataOrderDetail) {
        $dataOrder['order_code'] = uniqid();
        $order = $this->orderRepo->create($dataOrder);
        if ($dataOrderDetail) {
            foreach ($dataOrderDetail as $data) {
                $dataOrder = array_merge(['order_id' => $order->id], ...$data);
                $product = $this->productRepo->findOrFail($dataOrder['product_id']);
                $product->quantity -= $dataOrder['quantity'];
                $product->save();
                $this->orderDetailRepo->create($dataOrder);
            }
        } 
    }

    /**
     * Update the specified resource in storage.
     * @param $data, string id
     * @return mixed
     */
    public function update($dataOrder, $dataOrderDetail, string $id) {
        $order = $this->orderRepo->findOrFail($id);
        
        $oldOrderDetails = $order->order_detail;
        
        foreach($oldOrderDetails as $detail) {
            $this->productRepo->restoreProduct($detail->product_id, $detail->quantity);
        }
        $order->update($dataOrder);

        $this->orderDetailRepo->deleteWhere(['order_id' => $order->id]);
        if ($dataOrderDetail) {
            foreach ($dataOrderDetail as $data) {
                if ($data)
                $dataOrder = array_merge(['order_id' => $order->id], ...$data);
                $product = $this->productRepo->findOrFail($dataOrder['product_id']);
                if($product['quantity'] > $dataOrder['quantity']) {
                    $product->quantity -= $dataOrder['quantity'];
                }
                $product->save();
                $this->orderDetailRepo->create($dataOrder);
            }
        } 
    }
    /**
     * Get List Order Paginate
     * @param int $limit
     * @return mixed
     */
    public function getListPaginate($limit = 10)
    {
        return $this->orderRepo->paginate($limit);
    }

    /**
     * Get Order By Id
     * @param int $id
     * @return mixed
     */
    public function getOrderById($id)
    {
        return $this->orderRepo->findOrFail($id);
    }
}
