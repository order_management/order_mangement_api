<?php

namespace App\Http\Services;

use App\Http\Resources\ReturnOrderResource;
use App\Repositories\ReturnOrderRepository;

class ReturnOrderService
{
    protected $returnOrderService;

    public function __construct(ReturnOrderRepository $returnOrderService)
    {
        $this->returnOrderService = $returnOrderService;
    }

    /**
     * Get List Return Order
     * @param int
     * @return mixed
     */
    public function index()
    {
        $returnOrderService = $this->returnOrderService->get();
        return ReturnOrderResource::collection($returnOrderService);
    }

    /**
     * Get Detail Return Order
     * @param int
     * @return mixed
     */
    public function show($id)
    {
        $returnOrderService = $this->returnOrderService->findByField('id', $id);
        return ReturnOrderResource::collection($returnOrderService);
    }

    /**
     * Submit Return Order
     * @param int
     * @return mixed
     */
    public function store($params)
    {
        $returnOrder = $this->returnOrderService->create($params);
        return new ReturnOrderResource($returnOrder);
    }
}
