<?php

namespace App\Http\Services;

use App\Http\Resources\CustomerResource;
use App\Repositories\CustomerRepository;

class CustomerService
{
    protected $customerRepo;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepo = $customerRepo;
    }

    /**
     * Get Customer List
     * @param int
     * @return mixed
     */
    public function index()
    {
        $customer = $this->customerRepo->get();
        return CustomerResource::collection($customer);
    }

    /**
     * Get Customer Detail
     * @param int
     * @return mixed
     */
    public function show($id)
    {
        $customer = $this->customerRepo->findOrFail($id);
        return new CustomerResource($customer);
    }

    /**
     * Create Customer
     * @param param
     * @return mixed
     */
    public function store($params)
    {
        $customer = $this->customerRepo->create($params);
        return new CustomerResource($customer);
    }

    /**
     * Update Customer
     * @param param
     * @return mixed
     */
    public function update($customer, $params)
    {
        $customer->update($params);
        return new CustomerResource($customer);
    }

}
