<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnOrder extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_detail_id',
        'quantity',
        'total_price',
        'note',
    ];

    /**
     * Get OrderDetail
     */
    public function order_detail()
    {
        return $this->belongsTo(OrderDetail::class);
    }

}
