<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'product_id',
        'product_price',
        'quantity',
        'status',
        'total_price',
        'discount',
    ];

    /**
     * Get Order
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * Get Product
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get ReturnOrder
     */
    public function return_order()
    {
        return $this->hasMany(ReturnOrder::class, 'order_detail_id');
    }

}
